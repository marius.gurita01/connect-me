/** @type {import('tailwindcss').Config} */
const config = {
  mode: 'jit',
  content: ["./src/**/*.{js,ts,jsx,tsx}"],
  theme: {
    extend: {
      colors: {
        first: '#2596be',
        second: "#AED4F8",
        teal: '#008080',
      },
    },
  },
  plugins: [require('@tailwindcss/line-clamp'), require('@tailwindcss/typography')],
};

module.exports = config;
