import React, { useState } from "react";

interface AnswerFormProps {
  onSubmit: (content: string) => void;
}

const AnswerForm: React.FC<AnswerFormProps> = ({ onSubmit }) => {
  const [answerContent, setAnswerContent] = useState("");

  return (
    <div>
      <label className="mb-2 block text-sm font-medium text-gray-700">
        Your Answer
      </label>
      <textarea
        className="w-full rounded border px-3 py-2 text-gray-700 focus:outline-none shadow appearance-none leading-tight focus:shadow-outline"
        rows={4}
        value={answerContent}
        onChange={(e) => setAnswerContent(e.target.value)}
      ></textarea>
      <button
        className="mt-4 bg-purple-600 hover:bg-purple-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
        onClick={() => {
          onSubmit(answerContent);
          setAnswerContent("");
        }}
      >
        Submit Answer
      </button>
    </div>
  );

};

export default AnswerForm;
