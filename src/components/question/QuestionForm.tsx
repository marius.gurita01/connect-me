import type { ChangeEvent, FormEvent } from "react";
import { useState, useRef } from "react";
import { api } from "~/utils/api";
import BackButton from "../BackButton";
import { useRouter } from "next/router";
import { useUpload } from "~/hooks";

function QuestionForm() {
  const apicontext = api.useContext();
  const createQuestion = api.question.createQuestion.useMutation({
    async onSettled() {
      await apicontext.question.getAllQuestions.invalidate();
    },
  });
  const createFile = api.file.createFile.useMutation();
  const router = useRouter();
  const currentPath = router.asPath;
  const [formData, setFormData] = useState<{
    title: string;
    content: string;
    file?: File;
  }>({
    title: "",
    content: "",
  });

  const handleChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    try {
      console.log(e.currentTarget);
      const serverId = currentPath.split("/")[2] as string;
      const question = await createQuestion.mutateAsync({
        ...formData,
        serverId,
      });
      const file = formData.file;
      if (!file) return;
      const Key = `question/${question.id}/${file.name}`;

      const res = await upload({ file, Key });
      if (res.status != 200) {
        console.log(res);
        return;
      }

      const url = `https://connect-me-s3-bucket.s3.eu-central-1.amazonaws.com/${Key}`;
      const s3file = await createFile.mutateAsync({
        key: Key,
        type: file.type,
        questionId: question.id
      });

      router.back();
    } catch (err) {
      console.log(err);
    }
  };
  const [file, setFile] = useState<File>();
  const { upload, error, isError } = useUpload();

  const handleChangeFile: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    if (!e.target.files) return;
    console.log(e.target.files);
    const file = Array.from(e.target.files).pop();
    if (!file) return;
    setFormData({ ...formData, file });
  };
  return (
    <div className="flex min-h-screen bg-white items-center justify-center">
      <div className="w-2/3 bg-white shadow-md rounded px-8 pt-6 pb-8 mb-4">
        <h1 className="mb-6 text-3xl font-bold text-gray-600">
          Ask a question
        </h1>
        <form onSubmit={handleSubmit}>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="title"
            >
              Question title
            </label>
            <input
              className="shadow appearance-none border rounded w-full py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="title"
              type="text"
              name="title"
              value={formData.title}
              onChange={handleChange}
              placeholder="Enter question title"
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="block text-gray-700 text-sm font-bold mb-2"
              htmlFor="content"
            >
              Question content
            </label>
            <textarea
              className="shadow appearance-none border rounded w-full h-48 py-2 px-3 text-gray-700 leading-tight focus:outline-none focus:shadow-outline"
              id="content"
              name="content"
              value={formData.content}
              onChange={handleChange}
              placeholder="Enter question content"
              required
            />
          </div>
          <input type="file" onChange={handleChangeFile}></input>
          {isError && <div className="text-red-500 text-xs italic">{JSON.stringify(error) ?? "Error"}</div>}
          <div className="flex items-center justify-center mt-4">
            <button
              className="bg-gray-600 hover:bg-gray-700 text-white font-bold py-2 px-4 rounded focus:outline-none focus:shadow-outline"
              type="submit"
            >
              Submit
            </button>
          </div>
        </form>
      </div>
      <BackButton />
    </div>
  );

}

export default QuestionForm;
