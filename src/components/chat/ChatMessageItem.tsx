import type { Message } from "@prisma/client";
import React from "react";
import { api } from "~/utils/api";
import Image from "next/image";
import { formatDistanceToNow, isValid, parseJSON } from "date-fns";
import { enUS } from "date-fns/locale";
import ReactMarkdown from "react-markdown";
import rehypeHighlight from "rehype-highlight";
import "highlight.js/styles/atom-one-light.css";

export const ChatMessageItem: React.FC<{ msg: Message }> = ({ msg }) => {
  const user = api.user.getUserById.useQuery({ id: msg.userId });

  const formatDate = (timestamp: string | Date) => {
    if (typeof timestamp === "string") {
      const date = parseJSON(timestamp);
      if (isValid(date)) {      
        return formatDistanceToNow(date, { addSuffix: true, locale: enUS });
      }
    } else if (timestamp instanceof Date && isValid(timestamp)) {
      return formatDistanceToNow(timestamp, { addSuffix: true, locale: enUS });
    }
    return "wrong";
  };

  const formattedTimestamp = formatDate(msg.timestamp);

  return (
    <div className="p-5 bg-gray-50 shadow rounded-lg flex items-start hover:shadow-lg transition-all duration-200 ease-in-out mb-4">
      {user.data?.image && (
        <Image
          alt="Profile picture"
          src={user.data.image}
          width={50}
          height={50}
          className="rounded-full object-cover mr-5"
        />
      )}

      <div >
        <div className="flex items-center mb-2">
          <span className="text-lg text-gray-900 font-semibold">
            {user.data?.name}
          </span>
          {formattedTimestamp && (
            <span className="text-sm text-gray-500 ml-3">
              {formattedTimestamp}
            </span>
          )}
        </div>

        <div
          className="text-sm text-gray-700"
          style={{ wordWrap: "break-word" }}
        >
          <div className="mt-1 text-gray-900 markdown">
            <ReactMarkdown rehypePlugins={[rehypeHighlight]}>
              {msg.content}
            </ReactMarkdown>
          </div>
        </div>
      </div>
    </div>
  );
};
