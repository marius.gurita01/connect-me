import React, { useState } from "react";
import { v4 as uuidv4 } from "uuid";
import type { Message } from "@prisma/client";
import { HiOutlinePaperAirplane, HiOutlinePaperClip } from "react-icons/hi";

const ChatInput: React.FC<{
  onSubmit: (chatMessage: Message, file?: File) => void;
  userId: string;
  serverId: string;
}> = ({ onSubmit, userId, serverId }) => {
  const [text, setText] = useState<string>("");
  const [file, setFile] = useState<File>();

  const handleFileChange: React.ChangeEventHandler<HTMLInputElement> = (
    e: React.ChangeEvent<HTMLInputElement>
  ) => {
    if (e.target.files?.length) {
      setFile(e.target.files[0]);
    }
  };

  const handleSubmit: React.FormEventHandler<HTMLFormElement> = (
    e: React.FormEvent<HTMLFormElement>
  ) => {
    e.preventDefault();
    const chatMessage: Message = {
      content: text,
      id: uuidv4(),
      timestamp: new Date(),
      userId,
      serverId,
      fileS3Id: null
    };
    onSubmit(chatMessage, file);
    setText("");
    setFile(undefined);
  };

  return (
    <form onSubmit={handleSubmit} className="fixed bottom-0 left-0 right-0 z-10 rounded-xl bg-gray-200 p-4 flex items-center space-x-4">
      <div className="flex items-center justify-center w-full gap-3">
        <textarea
            value={text}
            onChange={(e) => setText(e.currentTarget.value)}
            placeholder="Type a message"
            rows={2}
            className="max-h-28 flex-grow py-2 px-4 rounded-md bg-gray-100 w-full text-gray-700 placeholder-gray-600 focus:outline-none"
        ></textarea>
        <input 
            type="file"
            onChange={handleFileChange}
            className="hidden"
            id="fileInput"
        />
        <label htmlFor="fileInput" className="px-4 py-2 flex justify-center items-center rounded-md bg-gray-600  hover:bg-gray-800 text-white focus:outline-none cursor-pointer">
          <HiOutlinePaperClip className="h-5 w-5" />
        </label>
        <button
            type="submit"
            className=" px-4 py-2 flex justify-center items-center rounded-md bg-gray-600  hover:bg-gray-800 text-white focus:outline-none"
        >
            <HiOutlinePaperAirplane className="h-5 w-5" />
        </button>
      </div>
    </form>
  );
};

export default ChatInput;
