// ~/components/Layout.tsx
import React from "react";
import HeaderBar from "./HeaderBar";

interface LayoutProps {
  children: React.ReactNode;
}

const Layout: React.FC<LayoutProps> = ({ children }) => {
  return (
    <div className="flex flex-col h-screen">
      <HeaderBar />
      <div className="flex-grow overflow-auto pt-20">
        {children}</div>
    </div>
  );
};

export default Layout;

const getLayout = (page: React.ReactElement) => {
  return <Layout>{page}</Layout>;
};

export { getLayout };
