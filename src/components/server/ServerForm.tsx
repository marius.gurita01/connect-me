import router from "next/router";
import type { ChangeEvent, FormEvent } from "react";
import { useState } from "react";
import { api } from "~/utils/api";
import BackButton from "../BackButton";
import { Json } from "aws-sdk/clients/robomaker";

function ServerForm() {
  const apicontext = api.useContext();
  const createServer = api.server.createServer.useMutation({
    async onSettled() {
      await apicontext.server.getAllServers.invalidate();
    },
  });

  const [selectedSemester, setSelectedSemester] = useState("1");
  const [selectedYear, setSelectedYear] = useState("1");

  const [formData, setFormData] = useState({
    name: "",
    description: "",
    faculty: "",
    year: 1,
    semester: 1,
    importance: "",
    additionalInfo: "",
    usefulLinks: "",
  });
  const [links, setLinks] = useState([""]);

  const handleLinkChange = (index: number, newUrl: string) => {
    setLinks((oldLinks) =>
      oldLinks.map((link, i) => (i === index ? newUrl : link))
    );
    console.log(links);
    
  };

  const addLink = (event: React.MouseEvent) => {
    event.preventDefault();
    setLinks((oldLinks) => [...oldLinks, ""]);
  };

  const removeLink = (index: number, event: React.MouseEvent) => {
    event.preventDefault();
    setLinks((oldLinks) => oldLinks.filter((_, i) => i !== index));
  };

  const handleChange = (
    event: ChangeEvent<HTMLInputElement | HTMLTextAreaElement>
  ) => {
    const { name, value } = event.target;
    setFormData((prevData) => ({
      ...prevData,
      [name]: value,
    }));
  };

  const handleSubmit = async (e: FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const uLinks = links.join('\n')
    createServer.mutate({
      ...formData,
      usefulLinks: uLinks,
      year: parseInt(selectedYear),
      semester: parseInt(selectedSemester),
    });
    console.log(formData);
    router.back()
  };
  return (
    <div className="flex min-h-screen items-center justify-center bg-gray-100">
      <div className="w-full max-w-xl px-6 py-8 md:p-10 my-6 bg-white rounded-lg shadow-md">
        <h1 className="text-2xl font-bold text-center mb-4 text-gray-700">
          Course Form
        </h1>
        <form onSubmit={handleSubmit} className="space-y-6">
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="name"
            >
              Course Name
            </label>
            <input
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
              id="name"
              type="text"
              name="name"
              value={formData.name}
              onChange={handleChange}
              placeholder="Enter course name"
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="description"
            >
              Course Description
            </label>
            <textarea
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
              id="description"
              name="description"
              value={formData.description}
              onChange={handleChange}
              placeholder="Enter course description"
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="faculty"
            >
              Faculty
            </label>
            <input
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
              id="faculty"
              type="text"
              name="faculty"
              value={formData.faculty}
              onChange={handleChange}
              placeholder="Enter faculty"
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="year"
            >
              Year
            </label>
            <select
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
              id="year"
              name="year"
              value={selectedYear}
              onChange={(e) => setSelectedYear(e.target.value)}
              required
            >
              <option value="1">1</option>
              <option value="2">2</option>
              <option value="3">3</option>
              <option value="4">4</option>
              <option value="5">5</option>
              <option value="6">6</option>
            </select>
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="semester"
            >
              Semester
            </label>
            <select
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
              id="semester"
              name="semester"
              value={selectedSemester}
              onChange={(e) => setSelectedSemester(e.target.value)}
              required
            >
              <option value="1">1</option>
              <option value="2">2</option>
            </select>
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="importance"
            >
              Importance
            </label>
            <textarea
              id="importance"
              name="importance"
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none max-h-36"
              value={formData.importance}
              onChange={handleChange}
              placeholder="Enter why this course is important"
              required
            />
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="additionalInfo"
            >
              Additional Information
            </label>
            <textarea
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none max-h-36"
              id="additionalInfo"
              name="additionalInfo"
              value={formData.additionalInfo}
              onChange={handleChange}
              placeholder="Enter additional information (optional)"
            />
          </div>
          <div className="mb-4">
            <label
              className="text-sm font-bold text-gray-600 block"
              htmlFor="usefulLinks"
            >
              Useful Links
            </label>
            {links.map((link, index) => (
              <div key={index} className="flex items-center space-x-2 mb-2">
                <input
                  className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
                  type="url"
                  id={`usefulLink${index}`}
                  name={`usefulLink${index}`}
                  value={link}
                  onChange={(event) =>
                    handleLinkChange(index, event.target.value)
                  }
                  placeholder="Enter useful link"
                />
                <button onClick={(event) => removeLink(index, event)}>-</button>
              </div>
            ))}
            <button
              onClick={addLink}
              className="w-full px-4 py-2 mt-2 text-gray-600 border rounded-lg focus:border-gray-600 focus:outline-none"
            >
              + Add another link
            </button>
          </div>
          <button
            className="w-full py-2 px-4 text-center bg-gray-800 rounded-lg text-white hover:bg-gray-900 focus:outline-none"
            type="submit"
          >
            Submit
          </button>
        </form>
        <div className="flex justify-center mt-6">
          <BackButton />
        </div>
      </div>
    </div>
  );
}
export default ServerForm;
