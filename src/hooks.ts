import { useEffect, useState } from "react";
import axios, { AxiosRequestConfig } from "axios";
import { api } from "./utils/api";
import { useMutation } from "@tanstack/react-query";
import { fileURLToPath } from "url";
import { AWSError } from "aws-sdk";

export const useDebouncedValue = <T>(value: T, time = 250) => {
  const [debouncedValue, setDebouncedValue] = useState<T>(value);

  useEffect(() => {
    const timeout = setTimeout(() => {
      setDebouncedValue(value);
    }, time);
    return () => {
      clearTimeout(timeout);
    };
  }, [value, time]);
  return debouncedValue;
};
const uploadFileMutationFn = async ({
  file,
  url,
  config,
}: {
  file: File;
  url: string;
  config?: AxiosRequestConfig<File> | undefined;
}) => {
  console.log("Uploading " + file.name + " to " + url);

  const res = await axios.put(url, file, config);
  console.log("🚀 ~ file: hooks.ts:230 ~ res:", res);

  return res;
};

export const useUpload = () => {
  const { mutateAsync: createPutPresignedURL } =
    api.aws.createPutPresignedURL.useMutation();
  const uploadFileMutation = useMutation({ mutationFn: uploadFileMutationFn });
  const upload = async ({ file, Key }: { Key: string, file: File }) => {
    const url = await createPutPresignedURL({ Key, Type: file.type })
    return uploadFileMutation.mutateAsync({
      file, url, config: {
        headers: {
          "Content-Type": file.type,
        },
      }
    })
  }
  return {
    upload,
    isSuccess: uploadFileMutation.isSuccess,
    isError: uploadFileMutation.isError,
    error: uploadFileMutation.error as AWSError,
  };
}