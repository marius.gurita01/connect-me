export const EVENTS = {
    "RECEIVE": "receive_message",
    "JOIN_ROOM": "join_room",
    "SEND": "send_message" 
}
export interface ChatMessage { 
    id: string;
    text: string;
    timestamp: Date;
    sender: string;
}