import { createTRPCRouter } from "~/server/api/trpc";
import { exampleRouter } from "~/server/api/routers/example";
import { serverRouter } from "./routers/serverRouter";
import { questionRouter } from "./routers/questionRouter";
import { userRouter } from "./routers/userRouter";
import { answerRouter } from "./routers/answearRouter";
import { reviewRouter } from "./routers/reviewRouter";
import { messageRouter } from "./routers/messageRouter";
import { awsRouter } from "./routers/awsRouter";
import { fileRouter } from "./routers/s3";

/**
 * This is the primary router for your server.
 *
 * All routers added in /api/routers should be manually added here.
 */
export const appRouter = createTRPCRouter({
  example: exampleRouter,
  server: serverRouter,
  question: questionRouter,
  user: userRouter,
  message: messageRouter,
  answer: answerRouter,
  review: reviewRouter,
  aws: awsRouter,
  file: fileRouter
});

// export type definition of API
export type AppRouter = typeof appRouter;
