import { createTRPCRouter, protectedProcedure } from "../trpc";
import { env } from "~/env.mjs";
import {
  S3Client,
  PutObjectCommand,
  GetObjectCommand,
  DeleteObjectCommand,
} from "@aws-sdk/client-s3";
import { getSignedUrl } from "@aws-sdk/s3-request-presigner";

import AWS from "aws-sdk";
import { z } from "zod";
const Bucket = "connect-me-s3-bucket"
const client = new S3Client({
  region: "eu-central-1", credentials: {
    accessKeyId: env.AWS_USERNAME,
    secretAccessKey: env.AWS_PASSWORD,
  }
});
export const awsRouter = createTRPCRouter({
  createPutPresignedURL: protectedProcedure
    .input(
      z.object({
        Key: z.string(),
        Type: z.string(),
      })
    )
    .mutation(async ({ input }) => {
      const { Key, Type } = input;

      console.log(Key, Type)
      const command = new PutObjectCommand({
        Bucket,
        Key,
        ContentType: Type,
      });
      const url = await getSignedUrl(client, command);
      return url;
    }),
  createDeleteObjectPresignedUrl: protectedProcedure
    .input(
      z.object({
        Bucket: z.string(),
        Key: z.string(),
      })
    )
    .mutation(async ({ input }) => {
      const { Bucket, Key } = input;
      const client = new S3Client({ region: "eu-central-1" });

      const command = new DeleteObjectCommand({
        Bucket,
        Key,
      });
      const url = await getSignedUrl(client, command);
      return url;
    }),
});
