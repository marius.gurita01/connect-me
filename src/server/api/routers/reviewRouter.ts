import { z } from "zod";
import { prisma } from "../../db"
import {
    createTRPCRouter,
    protectedProcedure,
} from "~/server/api/trpc";
import { TRPCError } from "@trpc/server";

async function updateAverageRating(serverId: string, raiting: number): Promise<void> {
    const reviews = await prisma.review.findMany({
        where: {
            serverId: serverId,
        },
    });

    let sum = 0;

    for (const review of reviews) {
        sum += review.rating;
    }
    const average = Math.round((sum + raiting) / (reviews.length + 1))

    await prisma.server.update({
        where: { id: serverId },
        data: { rating: average },
    });
}

export const reviewRouter = createTRPCRouter({

    createReview: protectedProcedure
        .input(
            z.object({ title: z.string(), rating: z.number(), description: z.string(), serverId: z.string() })
        )
        .mutation(async ({ ctx, input }) => {
            const oldReview = await prisma.review.deleteMany({
                where: {
                    userId: ctx.session.user.id,
                }
            })
            const newReview = await prisma.review.create({
                data: {
                    title: input.title,
                    serverId: input.serverId,
                    description: input.description,
                    rating: input.rating,
                    userId: ctx.session.user.id,
                },
            });


            await updateAverageRating(input.serverId, input.rating);
            return newReview;
        }),

    getAllReviews: protectedProcedure
        .input(z.object({ serverId: z.string() }))
        .query(({ input }) => {
            return prisma.review.findMany({
                where: {
                    serverId: input.serverId,
                },
                include: {
                    User: {
                        select: {
                            name: true,
                            image: true
                        }
                    }
                },
            });
        }),
    getReviewById: protectedProcedure
        .input(z.object({ id: z.string() })).query(async ({
            input
        }) => {
            const review = await prisma.review.findFirst({ where: { id: input.id } })
            if (!review) {
                throw new TRPCError({
                    code: "BAD_REQUEST",
                    message: `Review not found (id ='(${input.id})' )`
                })

            }
            return review
        }),


});

