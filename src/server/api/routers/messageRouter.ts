import { z } from "zod";

import {
    createTRPCRouter,
    protectedProcedure,
} from "~/server/api/trpc";
import { prisma } from "~/server/db";

export const messageRouter = createTRPCRouter({

    createMessage: protectedProcedure.input(z.object({
        id: z.string(),
        content: z.string(),
        timestamp: z.date(),
        userId: z.string(),
        serverId: z.string(),
    })).mutation(async ({ input }) => {
        return await prisma.message.create({
            data: input
        })
    }),
    getAll: protectedProcedure.input(z.object({
        serverId: z.string()
    })).query(async ({ input }) => {
        return await prisma.message.findMany({
            where: input
        })
    })
});
