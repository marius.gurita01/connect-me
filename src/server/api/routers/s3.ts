import { z } from "zod";
import { prisma } from "../../db"
import {
    createTRPCRouter,
    protectedProcedure,
} from "~/server/api/trpc";
import { TRPCError } from "@trpc/server";

export const fileRouter = createTRPCRouter({
    createFile: protectedProcedure
        .input(z.object({ key: z.string(), type: z.string(), questionId: z.string().optional(), messageId: z.string().optional() }))
        .mutation(async ({ ctx, input }) => {
            return await ctx.prisma.fileS3.create({
                data: input,
            });
        }),
    
})