

import type { Server as HTTPServer } from 'http'
import type { NextApiRequest, NextApiResponse } from 'next'
import type { Socket as NetSocket } from 'net'
import { Server as IOServer } from 'socket.io'
import { EVENTS } from '~/utils/socket'
import {type Message } from '@prisma/client'

interface SocketServer extends HTTPServer {
    io?: IOServer | undefined
}

interface SocketWithIO extends NetSocket {
    server: SocketServer
}

interface NextApiResponseWithSocket extends NextApiResponse {
    socket: SocketWithIO
}
const SocketHandler = (req: NextApiRequest, res: NextApiResponseWithSocket) => {
    if (res.socket.server.io) {
        console.log('Socket is already running')
    } else {
        console.log('Socket is initializing')
        const io = new IOServer(res.socket.server)
        io.on("connection", socket => {
            socket.on(EVENTS.JOIN_ROOM, (room: string) => {
                socket.join(room)
            })
            socket.on(EVENTS.SEND, (room: string, chatMessage: Message) => {
                console.log(room, chatMessage);
                
                io.to(room).emit(EVENTS.RECEIVE, chatMessage)

            })
        })


        res.socket.server.io = io

    }
    res.end()
}


export default SocketHandler