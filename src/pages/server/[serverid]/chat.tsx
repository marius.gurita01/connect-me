import type { GetServerSideProps } from "next";
import { v4 as uuidv4 } from "uuid";
import React, { useEffect, useRef, useState } from "react";
import { getLayout } from "~/components/Layout";
import { type NextPageWithLayout } from "~/pages/_app";
import { EVENTS } from "~/utils/socket";
import { getServerAuthSession } from "~/server/auth";
import { api } from "~/utils/api";
import { type Socket, io } from "socket.io-client";
import { prisma } from "~/server/db";
import { type Message } from "@prisma/client";
import { timeStamp } from "console";
import { ChatMessageItem } from "~/components/chat/ChatMessageItem";
import ChatInput from "~/components/chat/ChatInput";
interface ChatPageProps {
  userId: string;
  serverId: string;
  messages: (Omit<Message, "timestamp"> & {
    timestamp: string;
  })[];
}
// const ChatInput: React.FC<{
//   onSubmit: (chatMessage: Message) => void;
//   userId: string;
//   serverId: string;
// }> = ({ onSubmit, userId, serverId }) => {
//   const [text, setText] = useState<string>("");
//   const handleSubmit: React.FormEventHandler<HTMLFormElement> = (
//     e: React.FormEvent<HTMLFormElement>
//   ) => {
//     e.preventDefault();
//     const chatMessage: Message = {
//       content: text,
//       id: uuidv4(),
//       timestamp: new Date(),
//       userId,
//       serverId,
//     };
//     onSubmit(chatMessage);
//     setText("")
//   };
//   return (
// <form onSubmit={handleSubmit} className="flex items-center">
//   <input
//     value={text}
//     onChange={(e) => setText(e.currentTarget.value)}
//     type="text"
//     className="py-3 px-4 border border-gray-300 rounded-lg bg-white text-gray-800 focus:outline-none focus:ring-2 focus:ring-blue-500 focus:border-transparent shadow-sm"
//     placeholder="Enter text"
//   />
//   <button
//     type="submit"
//     className="ml-4 py-3 px-6 bg-blue-500 hover:bg-blue-600 text-white rounded-lg focus:outline-none focus:ring-2 focus:ring-blue-500 focus:ring-offset-2 focus:ring-offset-white shadow-sm"
//   >
//     Submit
//   </button>
// </form>

//   );
// };

const Chat: NextPageWithLayout<ChatPageProps> = ({
  userId,
  serverId,
  messages,
}) => {
  const socketRef = useRef<Socket>();
  const createMessageMutation = api.message.createMessage.useMutation();
  const initialMessages: Message[] = messages.map((msg) => ({
    ...msg,
    timestamp: new Date(msg.timestamp),
  }));
  const [received, setReceived] = useState<Message[]>(initialMessages);
  const onReceive = (msg: Message) => {
    console.log(msg);

    setReceived((prev) => [...prev, msg]);
  };
  useEffect(() => {
    if (!serverId) return;
    fetch("/api/socket")
      .then(() => {
        if (!socketRef.current) {
          socketRef.current = io();
          socketRef.current.on(EVENTS.RECEIVE, onReceive);
          socketRef.current.on("connect", () => {
            console.log("connected");

            // Now fetch "/api/socketRef.current" after the connection is established and we're already listening for events
          });
          socketRef.current.emit(EVENTS.JOIN_ROOM, serverId);
          socketRef.current.connect();
          return;
        }
      })
      .catch((err) => console.log(err));
    return () => {
      if (socketRef.current) {
        socketRef.current.disconnect();
      }
    };
  }, [serverId]);
  const handleSubmitMessage = (chatMessage: Message) => {
    if (socketRef.current) {
      console.log("sending message");

      socketRef.current.emit(EVENTS.SEND, serverId, chatMessage);
      createMessageMutation.mutate(chatMessage);
    }
  };
  const messagesContainerRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    const messagesContainer = messagesContainerRef.current;
    if (messagesContainer) {
      messagesContainer.scrollTop = messagesContainer.scrollHeight;
    }
  }, [received]);

  return (
    <div className="grid grid-cols-10 rounded-lg bg-white ">
      <div className="col-start-2 col-span-8">
        <div
          ref={messagesContainerRef}
          className="messages-container mb-16" 
          style={{ height: "810px", overflowY: "auto" }}
        >
          {received.map((msg) => (
            <ChatMessageItem msg={msg} key={msg.id} />
          ))}
        </div>
        <div>
          <ChatInput
            serverId={serverId}
            userId={userId}
            onSubmit={handleSubmitMessage}
          />
        </div>
    </div>
    </div>
  );
};
Chat.getLayout = getLayout;
export const getServerSideProps: GetServerSideProps<ChatPageProps> = async (
  context
) => {
  const session = await getServerAuthSession(context);
  if (!session)
    return { redirect: { destination: "/server", permanent: true } };
  const { query } = context;
  const serverId = query.serverid as string;
  const messages = await prisma.message.findMany({
    where: { serverId },
  });
  const seriziableMessages = messages.map((msg) => ({
    ...msg,
    timestamp: msg.timestamp.toJSON(),
  }));
  return {
    props: { userId: session.user.id, serverId, messages: seriziableMessages },
  };
};
export default Chat;
