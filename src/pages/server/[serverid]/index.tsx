import Link from "next/link";
import { useRouter } from "next/router";
import type { ParsedUrlQuery } from "querystring";
import React from "react";
import { getLayout } from "~/components/Layout";
import Reviews from "~/components/server/Reviews";
import type { NextPageWithLayout } from "~/pages/_app";
import { api } from "~/utils/api";

interface QParams extends ParsedUrlQuery {
  serverid: string;
}

const ServerPage: NextPageWithLayout = () => {
  const deleteServer = api.server.deleteServerById.useMutation();

  const router = useRouter();
  const { serverid: id } = router.query as QParams;
  const server = api.server.getServerById.useQuery({ id });
  const Spinner = () => (
    <div className="h-16 w-16 animate-spin rounded-full border-t-4 border-solid border-blue-500"></div>
  );

  if (!server.data) {
    return (
      <div className="flex items-center justify-center min-h-screen bg-gray-100">
        <Spinner />
      </div>
    );
  }

  const handleDeleteClick = async () => {
    if (window.confirm("Are you sure you want to delete this server?")) {
      await deleteServer.mutate({ id });
      router.push("/server");
    }
  };

  return (
    <div className="mx-auto my-4 w-full max-w-3xl bg-white rounded-lg p-6">
      <h2 className="mb-4 text-3xl font-semibold text-gray-900">
        {server.data.name}
      </h2>
      <p className="mb-4 text-gray-700">{server.data.description}</p>
      <div className="grid grid-cols-2 gap-4 mb-6">
        <div>
          <p className="font-semibold text-gray-700">Faculty</p>
          <p>{server.data.faculty}</p>
        </div>
        <div>
          <p className="font-semibold text-gray-700">Year & Semester</p>
          <p>
            {server.data.year} - {server.data.semester}
          </p>
        </div>
      </div>
      <div className="mb-6">
        <p className="font-semibold text-gray-700">Rating</p>
        <p className="text-2xl font-semibold text-blue-600">{server.data.rating}/5</p>
      </div>
      <div className="mb-6">
        <p className="font-semibold text-gray-700">Importance</p>
        <p>{server.data.importance}</p>
      </div>
      {server.data.additionalInfo && (
        <div className="mb-6">
          <p className="font-semibold text-gray-700">Additional Information</p>
          <p>{server.data.additionalInfo}</p>
        </div>
      )}
      {server.data.usefulLinks && (
        <div className="mb-6">
          <p className="font-semibold text-gray-700">Useful Links</p>
          <a
            href={server.data.usefulLinks}
            target="_blank"
            rel="noopener noreferrer"
            className="text-blue-600 hover:underline"
          >
            {server.data.usefulLinks}
          </a>
        </div>
      )}
      <div className="flex justify-between mb-6">
        <Link href={`/server/${server.data.id}/review`}>
          <button className="focus:outline-none focus:shadow-outline rounded-lg bg-blue-600 hover:bg-blue-700 text-white font-semibold py-2 px-4">
            Review
          </button>
        </Link>
        <button
          className="focus:outline-none focus:shadow-outline rounded-lg bg-red-600 hover:bg-red-700 text-white font-semibold py-2 px-4"
          onClick={handleDeleteClick}
        >
          Delete
        </button>
      </div>
      <Reviews id={id} />
    </div>
  );
};

ServerPage.getLayout = getLayout;
export default ServerPage;
