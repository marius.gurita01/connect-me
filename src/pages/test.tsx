import React, { ChangeEvent, useState } from "react";
import { useUpload } from "~/hooks";

const Test = () => {
  const [file, setFile] = useState<File>();
  const { upload, error, isError } = useUpload();
  const handleChange: React.ChangeEventHandler<HTMLInputElement> = (e) => {
    if (!e.target.files) return;
    console.log(e.target.files);
    const file = Array.from(e.target.files).pop();
    if (!file) return;

    setFile(file);
  };
  const handleUpload = () => {
    if (!file) return;
    upload({ file, Key: file.name })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  return (
    <div>
      <input type="file" onChange={handleChange}></input>
      <button type="button" onClick={handleUpload}>Up</button>
      {isError && <div>{JSON.stringify(error) ?? "wtf"}</div>}
    </div>
  );
};

export default Test;
