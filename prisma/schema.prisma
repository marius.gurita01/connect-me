generator client {
  provider      = "prisma-client-js"
  binaryTargets = ["native", "debian-openssl-1.1.x", "debian-openssl-3.0.x", "linux-musl", "linux-musl-openssl-3.0.x"]
}

datasource db {
    provider          = "postgres"
    url               = env("DATABASE_URL")
    shadowDatabaseUrl = env("SHADOW_DATABASE_URL")
}

model Example {
    id        String   @id @default(cuid())
    createdAt DateTime @default(now())
    updatedAt DateTime @updatedAt
}

model Account {
    id                String  @id @default(cuid())
    userId            String
    type              String
    provider          String
    providerAccountId String
    refresh_token     String?
    access_token      String?
    expires_at        Int?
    token_type        String?
    scope             String?
    id_token          String?
    session_state     String?
    user              User    @relation(fields: [userId], references: [id], onDelete: Cascade)

    @@unique([provider, providerAccountId])
    @@index([userId])
}

model Session {
    id           String   @id @default(cuid())
    sessionToken String   @unique
    userId       String
    expires      DateTime
    user         User     @relation(fields: [userId], references: [id], onDelete: Cascade)

    @@index([userId])
}

model User {
    id            String     @id @default(cuid())
    name          String?
    email         String?    @unique
    emailVerified DateTime?
    image         String?
    dataId        String?
    accounts      Account[]
    Answer        Answer[]
    Data          Data?
    Message       Message[]
    Question      Question[]
    Review        Review[]
    sessions      Session[]
}

model Data {
    id          String @id @default(cuid())
    userId      String @unique
    university  String
    faculty     String
    description String
    year        String
    age         String
    role        Role
    user        User   @relation(fields: [userId], references: [id], onDelete: Cascade)
}

model VerificationToken {
    identifier String
    token      String   @unique
    expires    DateTime

    @@unique([identifier, token])
}

model Server {
    id             String     @id @default(cuid())
    name           String
    description    String
    faculty        String
    year           Int
    semester       Int
    rating         Int        @default(5)
    importance     String
    additionalInfo String?
    usefulLinks    String?
    messages       Message[]
    question       Question[]
    reviews        Review[]
}

model Review {
    id          String @id @default(cuid())
    userId      String
    serverId    String
    rating      Int
    title       String
    description String
    Server      Server @relation(fields: [serverId], references: [id], onDelete: Cascade)
    User        User   @relation(fields: [userId], references: [id])

    @@index([serverId])
    @@index([userId])
}

model Question {
    id         String   @id @default(cuid())
    created_at DateTime @default(now())
    updated_at DateTime @default(now())
    title      String
    solved     Boolean  @default(false)
    content    String
    serverId   String
    userId     String
    Answer     Answer[]
    server     Server   @relation(fields: [serverId], references: [id], onDelete: Cascade)
    user       User     @relation(fields: [userId], references: [id])
    files      FileS3[]

    @@index([serverId])
    @@index([userId])
}

model Answer {
    id         String   @id @default(cuid())
    created_at DateTime @default(now())
    updated_at DateTime @default(now())
    questionId String
    userId     String
    content    String
    question   Question @relation(fields: [questionId], references: [id], onDelete: Cascade)
    user       User     @relation(fields: [userId], references: [id])

    @@index([questionId])
    @@index([userId])
}

model Message {
    id        String   @id
    content   String
    timestamp DateTime
    userId    String
    serverId  String
    user      User     @relation(fields: [userId], references: [id], onDelete: Cascade)
    server    Server   @relation(fields: [serverId], references: [id], onDelete: Cascade)
    file      FileS3?
    fileS3Id  String?  @unique

    @@index([userId])
    @@index([serverId])
}

enum Role {
    Teacher
    Student
}

model FileS3 {
    id         String    @id @default(cuid())
    key        String
    type       String
    Question   Question? @relation(fields: [questionId], references: [id])
    questionId String?
    Message    Message?  @relation(fields: [messageId], references: [id])
    messageId  String?   @unique
}
