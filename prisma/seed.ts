import { PrismaClient } from "@prisma/client";

const prisma = new PrismaClient()

async function main() {
  const sid = "clibzonrk0002lgvjjopdaxx2"
  const uid = "clijbidjo0000lgdmo6mzvs74"
//   const questionData = [
//     { title: 'Neural Networks', content: 'Can someone explain the purpose of an activation function in a neural network?', serverId: sid, userId: uid },
//     { title: 'TensorFlow Basics', content: 'How can I initialize a tensor with specific dimensions in TensorFlow?', serverId: sid, userId: uid },
//     { title: 'Supervised Learning', content: 'What is the difference between classification and regression in supervised learning?', serverId: sid, userId: uid },
//     { title: 'Keras Model', content: 'How does the compile method work in a Keras model?', serverId: sid, userId: uid },
//     { title: 'Deep Learning Layers', content: 'What is the role of a dropout layer in a deep learning model?', serverId: sid, userId: uid },
//     { title: 'Computer Vision', content: 'How can I use OpenCV to detect edges in an image?', serverId: sid, userId: uid },
//     { title: 'Natural Language Processing', content: 'What is the concept of tokenization in Natural Language Processing?', serverId: sid, userId: uid },
//     { title: 'AI Ethics', content: 'Why is bias a problem in AI and Machine Learning models?', serverId: sid, userId: uid },
//     { title: 'Reinforcement Learning', content: 'What is the difference between Q-learning and SARSA?', serverId: sid, userId: uid },
//     { title: 'GANs', content: 'What are Generative Adversarial Networks and how do they work?', serverId: sid, userId: uid },
// ]

const questionData = [
    { title: 'Python scikit-learn', content: 'What does the following scikit-learn code do in Python?\n```py\nfrom sklearn.ensemble import RandomForestClassifier\nclf = RandomForestClassifier(random_state=0)\nclf.fit(X_train, y_train)\n```', serverId: sid, userId: uid },
    { title: 'TensorFlow and Keras', content: 'Can someone explain this TensorFlow and Keras code?\n```py\nimport tensorflow as tf\nmodel = tf.keras.models.Sequential()\nmodel.add(tf.keras.layers.Dense(units=5, activation="relu"))\nmodel.add(tf.keras.layers.Dense(units=1, activation="sigmoid"))\nmodel.compile(optimizer="adam", loss="binary_crossentropy", metrics=["accuracy"])\n```', serverId: sid, userId: uid },
    { title: 'Python and Pandas', content: 'What does this line of Python code using the pandas library do?\n```py\nimport pandas as pd\ndf = pd.read_csv("data.csv")\nprint(df.head())\n```', serverId: sid, userId: uid },
    { title: 'OpenAI Gym', content: 'Can someone help me understand this Python code using OpenAI Gym?\n```py\nimport gym\nenv = gym.make("CartPole-v0")\nobservation = env.reset()\nfor _ in range(1000):\n  env.render()\n  action = env.action_space.sample() # take a random action\n  observation, reward, done, info = env.step(action)\n  if done:\n    observation = env.reset()\nenv.close()\n```', serverId: sid, userId: uid },
    { title: 'PyTorch', content: 'What does this PyTorch code do?\n```py\nimport torch\nx = torch.tensor([1.0])\ny = torch.tensor([2.0])\nz = x + y\nprint(z)\n```', serverId: sid, userId: uid },
]

  for (const question of questionData) {
    await prisma.question.create({
      data: question,
    })
  }

}

main()
  .catch((e) => {
    console.error(e)
    process.exit(1)
  })
  // eslint-disable-next-line @typescript-eslint/no-misused-promises
  .finally(async () => {
    await prisma.$disconnect()
  })
