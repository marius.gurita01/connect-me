/*
  Warnings:

  - A unique constraint covering the columns `[messageId]` on the table `FileS3` will be added. If there are existing duplicate values, this will fail.
  - A unique constraint covering the columns `[fileS3Id]` on the table `Message` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "FileS3" ADD COLUMN     "messageId" TEXT,
ADD COLUMN     "questionId" TEXT;

-- AlterTable
ALTER TABLE "Message" ADD COLUMN     "fileS3Id" TEXT;

-- CreateIndex
CREATE UNIQUE INDEX "FileS3_messageId_key" ON "FileS3"("messageId");

-- CreateIndex
CREATE UNIQUE INDEX "Message_fileS3Id_key" ON "Message"("fileS3Id");

-- AddForeignKey
ALTER TABLE "FileS3" ADD CONSTRAINT "FileS3_questionId_fkey" FOREIGN KEY ("questionId") REFERENCES "Question"("id") ON DELETE SET NULL ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "FileS3" ADD CONSTRAINT "FileS3_messageId_fkey" FOREIGN KEY ("messageId") REFERENCES "Message"("id") ON DELETE SET NULL ON UPDATE CASCADE;
