-- CreateTable
CREATE TABLE "FileS3" (
    "id" TEXT NOT NULL,
    "key" TEXT NOT NULL,
    "type" TEXT NOT NULL,

    CONSTRAINT "FileS3_pkey" PRIMARY KEY ("id")
);
